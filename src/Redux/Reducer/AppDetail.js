import { LOGIN} from '../constant'
const INITIAL_STATE = {
    isLogin:false,
    userData:{}
};

const AppDetail = (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case LOGIN:
            return {
                ...state,
                isLogin: action.isLogin,
                userData:action.userData
            }
        default:
            return state
    }
}
export default AppDetail;