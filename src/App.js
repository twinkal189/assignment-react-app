import React, { Component } from 'react';
import { connect } from 'react-redux';
import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom';
import 'antd/dist/antd.css'
import './index.css'

const loading = (
  <div className="pt-3 text-center">
    <div className="sk-spinner sk-spinner-pulse"></div>
  </div>
)

const Login = React.lazy(() => import('./container/Login'));
const Assignment = React.lazy(() => import('./container/Assignment'));
const ViewAssignment = React.lazy(() => import('./container/Assignment/view'));

class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <React.Suspense fallback={loading}>
          <Switch>
            <Route exact path="/" component={Login} />
            {this.props.isLogin ?
              <React.Fragment>
                <Route exact path="/login" component={Login} />
                <Route exact path="/assignment" component={Assignment} />
                <Route exact path="/view-assignment/:id" component={ViewAssignment} />
              </React.Fragment> :
              <Redirect to="/" />}
          </Switch>
        </React.Suspense>
      </BrowserRouter>
    );
  }
}
const mapStateToProps = (state) => {
  const userData = state.AppDetailReducer.userData
  const isLogin = state.AppDetailReducer.isLogin
  return { userData, isLogin };
};
export default connect(mapStateToProps, null)(App)

