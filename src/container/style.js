
import Styled from 'styled-components'
export const PageContainer = Styled.div`
    background: url('../Images/banner-bg2.jpg');
    .container{
        background: url('../Images/banner-bg2.jpg');
        /* padding-top: 100px; */
        /* height: 100vh; */
    }
    .custom-card{
        margin:  20px;
        border-radius: 10px;
    }
    .item-image{
        width: 140px;
        border-radius: 3px;
        height: 104px;
    }
    .item-price{
        color:#0692d6;
        
    }
    .modal-item-title{
        margin-top: 10px;
        margin-bottom:5px
    }
`
