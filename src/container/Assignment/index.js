import React from 'react'
import { Link } from 'react-router-dom'
import { Table, Row, Col, Input ,Button} from 'antd'
import axios from 'axios'
import global from '../../global'
import { Container } from './index.style'
import { connect } from 'react-redux';

import './style.css'
const columns = [
  {
    title: 'No',
    dataIndex: 'index',
    key: "index",
  },
  {
    title: 'Title',
    dataIndex: 'title',
    key: 'title',
  },
  {
    title: 'Subject',
    dataIndex: 'subject',
  },
  {
    title: 'Total Mark',
    dataIndex: 'totalMark',
    key: 'totalMark',
  },
  {
    title: 'Description',
    dataIndex: 'description',
    key: 'description',
  },
];
class Assignment extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      data: [],
      search: "",
      id:""
    }
  }
  componentDidMount() {
    this.listAPI()
  }
  listAPI() {
    let id=""
    if(this.props.isLogin){
      id=this.props.userData.id
    }
    let urlencoded = new URLSearchParams();
    urlencoded.append("studentId", id);
    urlencoded.append("search", this.state.search);
    axios
      .post(global.baseUrl + "getAssignmentList", urlencoded, {
        headers: { "Content-Type": "application/x-www-form-urlencoded" }
      })
      .then(result => {
        if (result.data.status) {
          let data = result.data.data
          let newData = []
          data.forEach((element, index) => {
            newData.push({
              key:index,
              index: index + 1,
              subject: <Link to={"/view-assignment/" + element.id}>{element.subject}</Link>,
              description: element.description,
              title: element.title,
              totalMark: element.totalMark
            })
          });
          this.setState({ data: newData })
        }
      });

  }
  handleChange = (e) => {
    this.setState({ search: e.target.value }, () => {
      this.listAPI()
    })
  }
  logout=()=>{
    this.props.dispatch({
      type: "LOGIN",
      userData: {},
      isLogin: false
    })
  }
  render() {
    return (
      <Container>
        <div className="login-div">
          <Row>
            <Col offset={20} span={4} className="logout-btn"><Button onClick={()=>this.logout()}>Logout</Button></Col>
          </Row>
          <Row className="main-content">
            <Col offset={4} span={5} className="search-ass "><div className="search-content"><p className="search-p">Search  </p>
              <Input style={{ height: "35px" }} value={this.state.search} name="search" onChange={(e) => this.handleChange(e)}
              /></div></Col>
          </Row>
          <Row>
            <Col span={16} offset={4}>
              <Table dataSource={this.state.data} columns={columns} />
            </Col>
          </Row>
        </div>
      </Container >
    )
  }
}
const mapStateToProps = (state) => {
  const userData = state.AppDetailReducer.userData
  const isLogin = state.AppDetailReducer.isLogin
  return { userData, isLogin };
};
export default connect(mapStateToProps, null)(Assignment)


