import React from 'react'
import { Row, Col, Card, Button, Input, notification } from 'antd'
import axios from 'axios'
import global from '../../global'
import { Container } from './index.style'
import { connect } from 'react-redux';
import {Link} from 'react-router-dom'
import './style.css'
let fileError = ""
let sendImage = ""
let isSubmit = false
const openNotification = () => {
    notification.open({
        message: 'Submitted',
        description:
            'Your answer submitted successfully ',

    });
};

class ViewPage extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            detail: {},
            answer: "",
            isActive: false,
            id: "",
            assignmentId: this.props.match.params.id
        }
        fileError = ""
        sendImage = ""
        isSubmit = false
    }

    componentDidMount() {
        this.listAPI()
    }
    listAPI() {
        let id = ""
        if (this.props.isLogin) {
            id = this.props.userData.id
        }
        this.setState({ id: id })
        let urlencoded = new URLSearchParams();
        urlencoded.append("studentId", id);
        urlencoded.append("id", this.props.match.params.id);
        axios
            .post(global.baseUrl + "getAssignmentDetail", urlencoded, {
                headers: { "Content-Type": "application/x-www-form-urlencoded" }
            })
            .then(result => {
                if (result.data.status) {
                    this.setState({ detail: result.data.data })
                    if (result.data.answer.length) {
                        this.setState({
                            answer: result.data.answer[0].answerFile,
                            isActive: true
                        })
                    }
                }
            });

    }
    onImageChange = (event) => {
        if (event.target.files && event.target.files[0]) {
            let extArray = event.target.files[0].name.split(".");
            let ext = extArray[extArray.length - 1];
            let flag = 0
            if (ext.toLowerCase() === 'pdf') {
                flag = 1
            }
            if (flag) {
                this.setState({ fileError: '' })
                fileError = ''
                let reader = new FileReader();
                reader.onload = (e) => {
                    // this.setState({ profileImage: e.target.result });
                };
                reader.readAsDataURL(event.target.files[0]);
                sendImage = event.target.files[0]
            }
            else {
                this.setState({
                    // profileImage:'',
                    fileError: "Please select only PDF"
                })
                fileError = "Please select only PDF"
                sendImage = ''
            }
        }
    }
    addFileHandler = () => {
        const apiUrl = global.baseUrl + "imageUpload";
        const formData = new FormData();
        formData.append("myFile", sendImage);
        const config = {
            headers: {
                "content-type": "multipart/form-data"
            }
        };
        axios
            .post(apiUrl, formData, config)
            .then(response => {
                this.setState({ answerFile: response.data.path });
                this.createAnswerAPICall();
            })
            .catch(error => { });

    }
    submitHandler = () => {
        this.setState({ isActive: false })
        if (sendImage === "") {
            fileError = "Please select file"
        } else if (sendImage && fileError === "") {
            this.addFileHandler()
        }
    }
    createAnswerAPICall = () => {
        let urlencoded = new URLSearchParams();
        urlencoded.append("studentId", this.state.id);
        urlencoded.append("assignmentId", this.state.assignmentId);
        urlencoded.append("answerFile", this.state.answerFile);

        axios
            .post(global.baseUrl + "createAnswer", urlencoded, {
                headers: { "Content-Type": "application/x-www-form-urlencoded" }
            })
            .then(result => {
                isSubmit = false
                this.setState({ isSubmit: false })
                if (result.data.status) {
                    openNotification()
                    this.props.history.push("/assignment");
                }
            });
    };
    logout = () => {
        this.props.dispatch({
            type: "LOGIN",
            userData: {},
            isLogin: false
        })
    }
    render() {
        return (
            <Container className="login-div">
                <Row>
                    <Col offset={20} span={4} className="logout-btn">
                        <Button style={{marginRight:"10px"}}><Link to="/assignment">Back</Link></Button>
                        <Button onClick={() => this.logout()}>Logout</Button></Col>
                </Row>

                <Row className="view-container">
                    <Col offset={6} span={12}>
                        <Card title="Assignment Detail" bordered={false} >
                            <Row className="detail-row">
                                <Col span={12}>Assignment</Col>
                                <Col span={12}>{this.state.detail.title}</Col>
                            </Row>
                            <Row className="detail-row">
                                <Col span={12}>Subject</Col>
                                <Col span={12}>{this.state.detail.subject}</Col>
                            </Row>
                            <Row className="detail-row">
                                <Col span={12}>Total Mark</Col>
                                <Col span={12}>{this.state.detail.totalMark}</Col>
                            </Row>
                            <Row className="detail-row">
                                <Col span={12}>Question Paper</Col>
                                <Col span={12}><a href={this.state.detail.questionPaper} target="_blank" rel="noopener noreferrer">Download Question Paper</a></Col>
                            </Row>
                            <Row className="detail-row">
                                <Col span={12}>Description</Col>
                                <Col span={12}>{this.state.detail.description}</Col>
                            </Row>
                            {this.state.answer ?
                                <Row className="detail-row">
                                    <Col span={12}>Your Answer</Col>
                                    <Col span={12}><a href={this.state.answer} target="_blank" rel="noopener noreferrer">Download Answer</a></Col>
                                </Row> :
                                <Row className="detail-row">
                                    <Col span={12}>Select Your Answer PDF File</Col>
                                    <Col span={12}>
                                        <Input type="file" onChange={(e) => this.onImageChange(e)} /><br />
                                        <p className="error-msg">{fileError}</p>
                                        <Button type="primary" htmlType="submit" className="submit-btn" onClick={() => this.submitHandler()} disabled={isSubmit}>
                                            Submit
                                        </Button>
                                    </Col>
                                </Row>
                            }
                        </Card>
                    </Col>
                </Row>

            </Container>
        )
    }
}
const mapStateToProps = (state) => {
    const userData = state.AppDetailReducer.userData
    const isLogin = state.AppDetailReducer.isLogin
    return { userData, isLogin };
  };
  export default connect(mapStateToProps, null)(ViewPage)
  