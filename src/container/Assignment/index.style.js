import Styled from 'styled-components'
export const Container = Styled.div`
    height:100vh;
    .main-content{
        padding-top:80px;
    }  
    .search-ass{
        margin-bottom:10px;
    } 
    .search-content{
        display:flex !important;
    }
    .search-p{
        font-weight:bold;
        margin-left:4px;
        margin-right:10px;
    }
    .view-container{
        padding-top:80px
    }
    .detail-row{
        padding:15px
    }
    .submit-btn{
        margin-top:10px
    }
    .error-msg{
        color:red;
        margin:0px;
        
    }
    .logout-btn{
        margin-top:50px;
        font-weight:bold;
        font-size:18px;
    }
    `
