import React from 'react'
import axios from 'axios'
import global from '../../global'
import { connect } from 'react-redux';

import { Form, Input, Button,  Alert, Row, Col } from 'antd';
import { LoginContainer } from './index.style'

import './login.css'
const layout = {
  labelCol: {
    span: 8,
  },
  wrapperCol: {
    span: 8,
  },
};
const tailLayout = {
  wrapperCol: {
    offset: 8,
    span: 16,
  },
};

class Login extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      passwordError: ""
    }
  }
  onFinish = (values) => {
    this.setState({ passwordError: "" })
    let urlencoded = new URLSearchParams();
    urlencoded.append("email", values.username);
    urlencoded.append("password", values.password);
    axios
      .post(global.baseUrl + "login", urlencoded, {
        headers: { "Content-Type": "application/x-www-form-urlencoded" }
      })
      .then(result => {
        if (result.data.status) {
          this.props.dispatch({
            type: "LOGIN",
            userData: result.data.data,
            isLogin: true
          })
          this.props.history.push("/assignment");
        }
        else {
          this.setState({
            passwordError: "Email and password are invalid"
          });
        }
      });
  };
  onFinishFailed = (errorInfo) => {
    this.setState({ passwordError: "" })
  };
  onClose = () => {
    this.setState({ passwordError: "" })
  }
  render() {
    return (
      <LoginContainer className="login-div">
        <h1 className="login-header">Start Your Session</h1>
        <Form
          {...layout}
          name="basic"
          initialValues={{
            remember: true,
          }}
          onFinish={this.onFinish}
          onFinishFailed={this.onFinishFailed}
          className="login-form"
        >
          {this.state.passwordError !== "" ? <Row>
            <Col span={8} offset={8}><Alert message={this.state.passwordError} type="error" {...layout}
              className="login-alert"
              closable
              onClose={this.onClose}
            /></Col>
          </Row>
            : null}
          <Form.Item
            label="Email"
            name="username"
            rules={[
              {
                required: true,
                message: 'Please input your username!',
              },
            ]}
          >
            <Input style={{ height: "40px" }} />
          </Form.Item>

          <Form.Item
            label="Password"
            name="password"
            rules={[
              {
                required: true,
                message: 'Please input your password!',
              },
            ]}
          >
            <Input.Password />

          </Form.Item>

          <Row>

            <Col offset={8} span={4}>
              <Button type="primary" htmlType="submit" className="login-btn">
                Login
              </Button>
            </Col>
          </Row>
          <Form.Item {...tailLayout}>
          </Form.Item>
        </Form>
      </LoginContainer>
    );
  }
};
const mapStateToProps = (state) => {
  const userData = state.AppDetailReducer.userData
  const isLogin = state.AppDetailReducer.isLogin
  return { userData, isLogin };
};
export default connect(mapStateToProps, null)(Login)
