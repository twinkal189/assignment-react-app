import Styled from 'styled-components'
export const LoginContainer = Styled.div`
    height:100vh;
    padding-top:100px;
    
    .login-header{
        text-align:center;
        // color:#1890ff;
        padding:15px;

    }
    .ant-input{
        height:30px;
        border-radius:3px;
    }
    .login-btn{
        width: 101px;
        height: 35px;
        font-size: 16px;
    }

    .login-form{
        // padding-top:100px;
    }
    .login-alert{
        margin-bottom:15px;
    }
    .ant-checkbox-wrapper{
        width:200px;
    }
`
